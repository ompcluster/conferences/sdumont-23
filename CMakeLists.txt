cmake_minimum_required(VERSION 3.10)

project(OMPCTutorial
    VERSION         0.1
    DESCRIPTION     "SDUMONT'23 OMPC Tutorial"
    LANGUAGES       C CXX)

set(CMAKE_C_STANDARD   99)
set(CMAKE_CXX_STANDARD 17)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

# Ensure we are using OMPC Clang
# ------------------------------------------------------------------------------

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
    execute_process(
        COMMAND ${CMAKE_C_COMPILER} --version
        OUTPUT_VARIABLE CLANG_VERSION)
    if(CLANG_VERSION MATCHES "^OmpCluster")
        message(STATUS
            "Using OMPC Clang version ${CMAKE_C_COMPILER_VERSION}!")
    else()
        message(FATAL_ERROR
            "It seems you are using vanilla Clang instead of OMPC Clang. "
            "Please check your installation and try again!")
    endif()
else()
    message(FATAL_ERROR
        "Not using OMPC Clang as compiler, found: ${CMAKE_C_COMPILER_ID}.\n"
        ""
        "Make sure to export the following variables and call CMake from within"
        " the container.\n"
        ""
        "    export CC=clang\n"
        "    export CXX=clang++")
endif()

# Target: OMPCRuntime
# ------------------------------------------------------------------------------

add_library(OMPC::Runtime IMPORTED INTERFACE)

set_property(
    TARGET OMPC::Runtime
    PROPERTY
        INTERFACE_COMPILE_OPTIONS -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu)

set_property(
    TARGET OMPC::Runtime
    PROPERTY
        INTERFACE_LINK_OPTIONS -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu)

# Subdirectories
# ------------------------------------------------------------------------------

add_subdirectory(examples)
add_subdirectory(hands-on)
